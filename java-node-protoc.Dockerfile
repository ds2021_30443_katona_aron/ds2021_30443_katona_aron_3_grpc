FROM adoptopenjdk/openjdk11:jre-11.0.13_8-alpine

RUN apk add --update npm wget

RUN wget https://github.com/protocolbuffers/protobuf/releases/download/v3.19.2/protoc-3.19.2-linux-x86_64.zip
RUN unzip protoc-3.19.2-linux-x86_64.zip -d protoc
RUN mv protoc/bin/protoc /protoc

ENV PATH "$PATH:/protoc"
