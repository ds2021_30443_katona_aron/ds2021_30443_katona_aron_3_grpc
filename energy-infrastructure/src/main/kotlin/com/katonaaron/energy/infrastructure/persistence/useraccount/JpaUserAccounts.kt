/*
 * Copyright (c) 2021 Áron Katona
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.katonaaron.energy.infrastructure.persistence.useraccount

import com.katonaaron.energy.domain.DDD
import com.katonaaron.energy.domain.useraccount.UserAccount
import com.katonaaron.energy.domain.useraccount.UserAccountId
import com.katonaaron.energy.domain.useraccount.UserAccounts
import com.katonaaron.energy.infrastructure.identity.UUIDGenerator
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional

@DDD.RepositoryImpl
@Repository
@Transactional
class JpaUserAccounts(
    private val repo: UserAccountRepositoryJpa,
    private val uuidGenerator: UUIDGenerator
) : UserAccounts {
    override fun nextIdentity(): UserAccountId = UserAccountId(uuidGenerator.generateUUID())

    override fun save(account: UserAccount): UserAccount = repo.saveAndFlush(account)

    override fun findAll(): Collection<UserAccount> = repo.findAll()

    override fun findById(id: UserAccountId): UserAccount? = repo.findById(id).orElse(null)

    override fun findByUsername(username: String): UserAccount? = repo.findByUsername(username)
    override fun delete(account: UserAccount) = repo.delete(account)

    override fun existsById(id: UserAccountId) = repo.existsById(id)

    override fun getNumberOfAccounts(): Long = repo.count()
}
