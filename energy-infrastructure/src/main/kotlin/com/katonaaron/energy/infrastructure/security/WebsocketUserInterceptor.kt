/*
 * Copyright (c) 2021 Áron Katona
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.katonaaron.energy.infrastructure.security

import io.jsonwebtoken.Jwts
import io.jsonwebtoken.security.Keys
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration
import org.springframework.core.Ordered
import org.springframework.core.annotation.Order
import org.springframework.messaging.Message
import org.springframework.messaging.MessageChannel
import org.springframework.messaging.simp.SimpMessageHeaderAccessor
import org.springframework.messaging.simp.config.ChannelRegistration
import org.springframework.messaging.simp.stomp.StompCommand
import org.springframework.messaging.simp.stomp.StompHeaderAccessor
import org.springframework.messaging.support.ChannelInterceptor
import org.springframework.messaging.support.MessageHeaderAccessor
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer

@Configuration
@Order(Ordered.HIGHEST_PRECEDENCE + 99)
class WebsocketUserInterceptor(
    private val userDetailsService: UserDetailsService
) : WebSocketMessageBrokerConfigurer {

    @Value("\${jwt.key}")
    private lateinit var jwtKey: String

    override fun configureClientInboundChannel(registration: ChannelRegistration) {
        registration.interceptors(object : ChannelInterceptor {
            override fun preSend(message: Message<*>, channel: MessageChannel): Message<*> {
                val accessor: StompHeaderAccessor? =
                    MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor::class.java)

                if (StompCommand.CONNECT == accessor?.command) {
                    val nativeHeaders = message.headers[SimpMessageHeaderAccessor.NATIVE_HEADERS] as Map<*, *>?
                    val header = nativeHeaders?.get("Authorization") as List<*>?
                    val token = header?.firstOrNull() as String?

                    token?.let {
                        val authentication = parseToken(token)
                        accessor.user = authentication
                    }

//                    println("message.headers[\"Authorization\"] = ${message.headers["Authorization"]}")
//                    println("message = ${message.headers}")
//                    println("nativeHeaders = ${nativeHeaders}")
//                    println("nativeHeaders.javaClass = ${nativeHeaders?.javaClass}")
//                    println("nativeHeaders[\"Authorization\"] = ${?.javaClass}")

//                Authentication user = ... ; // access authentication header(s)
//                accessor.setUser(user);
                }
                return message
            }
        })
    }

    private fun parseToken(token: String?): UsernamePasswordAuthenticationToken? {
        try {
            if (token != null && token.startsWith("Bearer ")) {
                val claims = Jwts
                    .parserBuilder()
                    .setSigningKey(Keys.hmacShaKeyFor(jwtKey.toByteArray()))
                    .build()
                    .parseClaimsJws(token.replace("Bearer ", ""))
                    .body
                return if (claims != null) {
                    val user = userDetailsService.loadUserByUsername(claims.subject)
                    UsernamePasswordAuthenticationToken(user, null, user.authorities)
                } else {
                    null
                }
            }
        } catch (e: io.jsonwebtoken.ExpiredJwtException) {
            println(e)
            return null
        }
        return null
    }
}
