/*
 * Copyright (c) 2021 Áron Katona
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.katonaaron.energy.infrastructure.security.jwt

import com.fasterxml.jackson.databind.ObjectMapper
import com.katonaaron.energy.domain.useraccount.UserAccount
import com.katonaaron.energy.domain.useraccount.UserRole
import com.katonaaron.energy.infrastructure.security.dto.LoginDto
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import io.jsonwebtoken.security.Keys
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import java.security.Key
import java.util.*
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class JwtAuthenticationFilter(
    private val _authenticationManager: AuthenticationManager,
    private val jwtAudience: String,
    private val jwtIssuer: String,
    private val jwtKey: String,
    private val jwtType: String,
    private val jwtExpiration: Long
) : UsernamePasswordAuthenticationFilter() {

    override fun attemptAuthentication(
        req: HttpServletRequest,
        res: HttpServletResponse
    ): Authentication {
        val (username, password) = ObjectMapper().readValue(req.inputStream, LoginDto::class.java)
        return _authenticationManager.authenticate(
            UsernamePasswordAuthenticationToken(
                username,
                password,
                ArrayList()
            )
        )
    }

    override fun successfulAuthentication(
        req: HttpServletRequest,
        res: HttpServletResponse,
        chain: FilterChain,
        auth: Authentication
    ) {
        val account = auth.principal as UserAccount
        val exp = Date(System.currentTimeMillis() + jwtExpiration)
        val key: Key = Keys.hmacShaKeyFor(jwtKey.toByteArray())
        val claims = Jwts
            .claims()
            .setIssuer(jwtIssuer)
            .setAudience(jwtAudience)
            .setSubject(account.username)
        claims["role"] = account.role.name
        if (account.role === UserRole.ROLE_CLIENT) {
            claims["clientId"] = account.client!!.id.value
        }
        val token = Jwts.builder()
            .setClaims(claims)
            .signWith(key, SignatureAlgorithm.HS512)
            .setHeaderParam("typ", jwtType)
            .setExpiration(exp)
            .compact()
        res.addHeader("X-Token", token)
        res.addHeader("Access-Control-Expose-Headers", "X-Token")
    }
}
