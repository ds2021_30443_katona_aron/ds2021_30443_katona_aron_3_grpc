/*
 * Copyright (c) 2021 Áron Katona
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.katonaaron.energy.infrastructure.persistence.client

import com.katonaaron.energy.domain.DDD
import com.katonaaron.energy.domain.client.Client
import com.katonaaron.energy.domain.client.ClientId
import com.katonaaron.energy.domain.client.Clients
import com.katonaaron.energy.infrastructure.identity.UUIDGenerator
import org.springframework.stereotype.Repository

@DDD.RepositoryImpl
@Repository
class JpaClients(
    private val repo: ClientRepositoryJpa,
    private val uuidGenerator: UUIDGenerator
) : Clients {
    override fun nextIdentity(): ClientId = ClientId(uuidGenerator.generateUUID())

    override fun save(client: Client): Client = repo.saveAndFlush(client)

    override fun findAll(): Collection<Client> = repo.findAll()

    override fun findById(id: ClientId): Client? = repo.findById(id).orElse(null)

    override fun delete(client: Client) = repo.delete(client)
}
