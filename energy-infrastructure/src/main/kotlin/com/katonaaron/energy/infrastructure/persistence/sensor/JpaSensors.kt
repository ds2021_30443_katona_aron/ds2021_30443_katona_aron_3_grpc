/*
 * Copyright (c) 2021 Áron Katona
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.katonaaron.energy.infrastructure.persistence.sensor

import com.katonaaron.energy.domain.DDD
import com.katonaaron.energy.domain.sensor.Sensor
import com.katonaaron.energy.domain.sensor.SensorId
import com.katonaaron.energy.domain.sensor.Sensors
import com.katonaaron.energy.infrastructure.identity.UUIDGenerator
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional

@DDD.RepositoryImpl
@Repository
@Transactional
class JpaSensors(
    private val repo: SensorRepositoryJpa,
    private val uuidGenerator: UUIDGenerator
) : Sensors {
    override fun nextIdentity(): SensorId = SensorId(uuidGenerator.generateUUID())

    override fun save(sensor: Sensor): Sensor = repo.saveAndFlush(sensor)

    override fun findAll(): Collection<Sensor> = repo.findAll()

    override fun findById(id: SensorId): Sensor? = repo.findById(id).orElse(null)

    override fun delete(sensor: Sensor) = repo.delete(sensor)

    override fun existsById(id: SensorId) = repo.existsById(id)
}
