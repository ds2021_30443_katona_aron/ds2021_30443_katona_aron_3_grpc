/*
 * Copyright (c) 2021 Áron Katona
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.katonaaron.energy.infrastructure.security.jwt

import io.jsonwebtoken.Jwts
import io.jsonwebtoken.security.Keys
import org.springframework.http.HttpHeaders
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter
import java.io.IOException
import javax.servlet.FilterChain
import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class JwtAuthorizationFilter(
    authManager: AuthenticationManager?,
    private val jwtKey: String,
    private val jwtIssuer: String,
    private val jwtType: String,
    private val jwtAudience: String,
    private val userDetailsService: UserDetailsService
) : BasicAuthenticationFilter(authManager) {
    @Throws(IOException::class, ServletException::class)
    override fun doFilterInternal(
        request: HttpServletRequest,
        response: HttpServletResponse,
        chain: FilterChain
    ) {
        val authentication = parseToken(request)
        if (authentication != null) {
            SecurityContextHolder.getContext().authentication = authentication
        } else {
            SecurityContextHolder.clearContext()
        }
        chain.doFilter(request, response)
    }

    private fun parseToken(request: HttpServletRequest): UsernamePasswordAuthenticationToken? {
        val token = request.getHeader(HttpHeaders.AUTHORIZATION)
        if (token != null && token.startsWith("Bearer ")) {
            val claims = Jwts
                .parserBuilder()
                .setSigningKey(Keys.hmacShaKeyFor(jwtKey.toByteArray()))
                .build()
                .parseClaimsJws(token.replace("Bearer ", ""))
                .body
            return if (claims != null) {
                val user = userDetailsService.loadUserByUsername(claims.subject)
                UsernamePasswordAuthenticationToken(user, null, user.authorities)
            } else {
                null
            }
        }
        return null
    }
}
