/*
 * Copyright (c) 2021 Áron Katona
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.katonaaron.energy.infrastructure.persistence.device

import com.katonaaron.energy.domain.DDD
import com.katonaaron.energy.domain.device.Device
import com.katonaaron.energy.domain.device.DeviceId
import com.katonaaron.energy.domain.device.Devices
import com.katonaaron.energy.infrastructure.identity.UUIDGenerator
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional

@DDD.RepositoryImpl
@Repository
@Transactional
class JpaDevices(
    private val repo: DeviceRepositoryJpa,
    private val uuidGenerator: UUIDGenerator
) : Devices {
    override fun nextIdentity(): DeviceId = DeviceId(uuidGenerator.generateUUID())

    override fun save(device: Device): Device = repo.saveAndFlush(device)

    override fun findAll(): Collection<Device> = repo.findAll()

    override fun findById(id: DeviceId): Device? = repo.findById(id).orElse(null)

    override fun delete(device: Device) = repo.delete(device)

    override fun existsById(id: DeviceId) = repo.existsById(id)
}
