image: openjdk:11-jdk-slim

variables:
  GRADLE_OPTS: "-Dorg.gradle.daemon=false"
  CI: ""

cache:
  paths:
    - .gradle/wrapper
    - .gradle/caches

before_script:
  - export GRADLE_USER_HOME=`pwd`/.gradle


stages:
  - all

smart-device-service:build:
  stage: all
  script:
    - ./gradlew energy-exposition:smart-device-service:ktlintCheck
    - ./gradlew energy-exposition:smart-device-service:build
  #    - ./gradlew jacocoTestReport
  artifacts:
    when: always
    reports:
      junit: energy-exposition/smart-device-service/build/test-results/test/**/TEST-*.xml
    paths:
      #      - energy-exposition/smart-device-service/build/reports/jacoco/test/jacocoTestReport.xml
      #      - energy-exposition/smart-device-service/build/reports/jacoco/test/html/index.html
      - energy-exposition/smart-device-service/build/libs/*.jar
    expire_in: 1 day

smart-device-service:build-container:
  stage: all
  needs: [ 'smart-device-service:build' ]
  image: jdrouet/docker-with-buildx:stable
  services:
    - docker:dind
  variables:
    DOCKER_HOST: tcp://docker:2375/
    DOCKER_DRIVER: overlay2
    TAG_LATEST: $CI_REGISTRY_IMAGE/smart-device-service:latest
    TAG_COMMIT: $CI_REGISTRY_IMAGE/smart-device-service:$CI_COMMIT_SHORT_SHA
  script:
    - docker login -u $CI_DEPLOY_USER -p $CI_DEPLOY_PASSWORD $CI_REGISTRY
    - docker pull $TAG_LATEST || true
    - docker buildx create --use
    - >-
      docker buildx build --push --platform linux/arm64/v8,linux/amd64 
      --cache-from $TAG_LATEST --tag $TAG_COMMIT
      --tag $TAG_LATEST --build-arg DB_IP=$DB_IP --build-arg DB_PORT=$DB_PORT 
      --build-arg DB_USER=$DB_USER --build-arg DB_PASSWORD=$DB_PASSWORD --build-arg DB_DBNAME=$DB_DBNAME 
      --build-arg JWT_KEY=$JWT_KEY   energy-exposition/smart-device-service
  only:
    - main

smart-device-service:deploy:
  stage: all
  needs: [ 'smart-device-service:build-container' ]
  image: alpine:latest
  tags:
    - deployment
  variables:
    TAG_COMMIT: $CI_REGISTRY_IMAGE/smart-device-service:$CI_COMMIT_SHORT_SHA
    CONTAINER: smart-device-service
  script:
    - chmod 600 $ID_RSA
    - apk update && apk add openssh-client
    - ssh -i $ID_RSA -o StrictHostKeyChecking=no $SERVER_USER@$SERVER_IP "docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY"
    - ssh -i $ID_RSA -o StrictHostKeyChecking=no $SERVER_USER@$SERVER_IP "docker pull $TAG_COMMIT"
    - ssh -i $ID_RSA -o StrictHostKeyChecking=no $SERVER_USER@$SERVER_IP "docker container rm -f $CONTAINER || true"
    - ssh -i $ID_RSA -o StrictHostKeyChecking=no $SERVER_USER@$SERVER_IP "docker run -d -p 9090:9090 --env PORT=9090 --name $CONTAINER $TAG_COMMIT"
  environment:
    name: production-smart-device-service
    url: $SMART_DEVICE_SERVICE_URL
  when: manual
  only:
    - main

envoy:build-container:
  stage: all
  image: jdrouet/docker-with-buildx:stable
  services:
    - docker:dind
  variables:
    DOCKER_HOST: tcp://docker:2375/
    DOCKER_DRIVER: overlay2
    TAG_LATEST: $CI_REGISTRY_IMAGE/envoy:latest
    TAG_COMMIT: $CI_REGISTRY_IMAGE/envoy:$CI_COMMIT_SHORT_SHA
  script:
    - docker login -u $CI_DEPLOY_USER -p $CI_DEPLOY_PASSWORD $CI_REGISTRY
    - docker pull $TAG_LATEST || true
    - docker buildx create --use
    - >-
      docker buildx build --push --platform linux/arm64/v8,linux/amd64 
      --cache-from $TAG_LATEST --tag $TAG_COMMIT
      --tag $TAG_LATEST envoy
  when: manual
  only:
    - main

envoy:deploy:
  stage: all
  needs: [ 'envoy:build-container' ]
  image: alpine:latest
  tags:
    - deployment
  variables:
    TAG_COMMIT: $CI_REGISTRY_IMAGE/envoy:$CI_COMMIT_SHORT_SHA
    CONTAINER: envoy
  script:
    - chmod 600 $ID_RSA
    - apk update && apk add openssh-client
    - ssh -i $ID_RSA -o StrictHostKeyChecking=no $SERVER_USER@$SERVER_IP "docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY"
    - ssh -i $ID_RSA -o StrictHostKeyChecking=no $SERVER_USER@$SERVER_IP "docker pull $TAG_COMMIT"
    - ssh -i $ID_RSA -o StrictHostKeyChecking=no $SERVER_USER@$SERVER_IP "docker container rm -f $CONTAINER || true"
    - >-
      ssh -i $ID_RSA -o StrictHostKeyChecking=no $SERVER_USER@$SERVER_IP "docker run -d --net=host 
      -e PORT=8080 -e SERVICE_HOST=localhost -e SERVICE_PORT=9090 --name $CONTAINER $TAG_COMMIT"
  environment:
    name: production-envoy
    url: $ENVOY_URL
  when: manual
  only:
    - main

smart-device-ui:build:
  stage: all
  image: registry.gitlab.com/ds2021_30443_katona_aron/ds2021_30443_katona_aron_3_grpc/java-node-protoc:latest
  script:
    - ./gradlew energy-ui:smart-device-ui:build
  artifacts:
    when: always
    paths:
      - energy-ui/smart-device-ui/dist
    expire_in: 1 day

smart-device-ui:build-container:
  stage: all
  needs:
    - smart-device-ui:build
  image: docker:latest
  services:
    - docker:dind
  variables:
    TAG_LATEST: $CI_REGISTRY_IMAGE/smart-device-ui:latest
    TAG_COMMIT: $CI_REGISTRY_IMAGE/smart-device-ui:$CI_COMMIT_SHORT_SHA
  script:
    - docker login -u $CI_DEPLOY_USER -p $CI_DEPLOY_PASSWORD $CI_REGISTRY
    - docker pull $TAG_LATEST || true
    - >-
      docker build --cache-from $TAG_LATEST --tag $TAG_COMMIT --tag $TAG_LATEST 
      --build-arg GRPC_SERVER_DOMAIN=${ENVOY_URL#https://} --build-arg AUTH_SERVER_DOMAIN=${ENERGY_DATA_URL#https://} 
      energy-ui/smart-device-ui
    - docker push $TAG_COMMIT
    - docker push $TAG_LATEST
  only:
    - main

smart-device-ui:deploy:
  stage: all
  needs: [ 'smart-device-ui:build-container' ]
  image: docker:latest
  services:
    - docker:dind
  variables:
    TAG_COMMIT: $CI_REGISTRY_IMAGE/smart-device-ui:$CI_COMMIT_SHORT_SHA
    TAG_HEROKU: registry.heroku.com/$SMART_DEVICE_UI_APP_NAME/web:latest
  script:
    - docker login -u $CI_DEPLOY_USER -p $CI_DEPLOY_PASSWORD $CI_REGISTRY
    - docker pull $TAG_COMMIT
    - docker tag $TAG_COMMIT $TAG_HEROKU
    - docker login --username=_ --password=$HEROKU_API_KEY $HEROKU_REGISTRY
    - docker push $TAG_HEROKU
    - docker run --rm -e HEROKU_API_KEY=$HEROKU_API_KEY wingrunr21/alpine-heroku-cli container:release web --app $SMART_DEVICE_UI_APP_NAME
  environment:
    name: production-smart-device-ui
    url: $SMART_DEVICE_UI_URL
  when: manual
  only:
    - main

sensor:build:
  stage: all
  image: openjdk:8-jdk-slim
  script:
    - ./gradlew :energy-sensor:build
  artifacts:
    when: always
    paths:
      - energy-sensor/build/distributions/*.tar
      - energy-sensor/build/distributions/*.zip
    expire_in: 1 day

#sensor:build-container:
#  stage: all
#  needs: [ 'sensor:build' ]
#  image: docker:latest
#  services:
#    - docker:dind
#  variables:
#    TAG_LATEST: $CI_REGISTRY_IMAGE/sensor:latest
#    TAG_COMMIT: $CI_REGISTRY_IMAGE/sensor:$CI_COMMIT_SHORT_SHA
#  script:
#    - docker login -u $CI_DEPLOY_USER -p $CI_DEPLOY_PASSWORD $CI_REGISTRY
#    - docker pull $TAG_LATEST || true
#    - >-
#      docker build --cache-from $TAG_LATEST --tag $TAG_COMMIT --tag $TAG_LATEST
#      --build-arg AMQP_URL=$QUEUE_URL --build-arg QUEUE_NAME=$QUEUE_NAME
#      --build-arg SENSOR_ID=$SENSOR_ID --build-arg SENSOR_DELAY=$SENSOR_DELAY
#      energy-sensor
#    - docker push $TAG_COMMIT
#    - docker push $TAG_LATEST
#  only:
#    - main

sensor:build-container:
  stage: all
  needs: [ 'sensor:build' ]
  image: jdrouet/docker-with-buildx:stable
  services:
    - docker:dind
  variables:
    DOCKER_HOST: tcp://docker:2375/
    DOCKER_DRIVER: overlay2
    TAG_LATEST: $CI_REGISTRY_IMAGE/sensor:latest
    TAG_COMMIT: $CI_REGISTRY_IMAGE/sensor:$CI_COMMIT_SHORT_SHA
  script:
    - docker login -u $CI_DEPLOY_USER -p $CI_DEPLOY_PASSWORD $CI_REGISTRY
    - docker pull $TAG_LATEST || true
    - docker buildx create --use
    - >-
      docker buildx build --push --platform linux/arm64/v8,linux/amd64 
      --cache-from $TAG_LATEST --tag $TAG_COMMIT
      --tag $TAG_LATEST --build-arg AMQP_URL=$QUEUE_URL --build-arg QUEUE_NAME=$QUEUE_NAME 
      --build-arg SENSOR_ID=$SENSOR_ID --build-arg SENSOR_DELAY=$SENSOR_DELAY 
      energy-sensor
  only:
    - main

#sensor:deploy:
#  stage: all
#  needs: [ 'sensor:build-container' ]
#  image: docker:latest
#  services:
#    - docker:dind
#  variables:
#    TAG_COMMIT: $CI_REGISTRY_IMAGE/sensor:$CI_COMMIT_SHORT_SHA
#    TAG_HEROKU: registry.heroku.com/$SENSOR_APP_NAME/web:latest
#  script:
#    - docker login -u $CI_DEPLOY_USER -p $CI_DEPLOY_PASSWORD $CI_REGISTRY
#    - docker pull $TAG_COMMIT
#    - docker tag $TAG_COMMIT $TAG_HEROKU
#    - docker login --username=_ --password=$HEROKU_API_KEY $HEROKU_REGISTRY
#    - docker push $TAG_HEROKU
#    - >-
#      docker run --rm -e HEROKU_API_KEY=$HEROKU_API_KEY wingrunr21/alpine-heroku-cli container:release web
#      --app $SENSOR_APP_NAME
#  when: manual
#  only:
#    - main

sensor:deploy:
  stage: all
  needs: [ 'sensor:build-container' ]
  image: alpine:latest
  tags:
    - deployment
  variables:
    TAG_COMMIT: $CI_REGISTRY_IMAGE/sensor:$CI_COMMIT_SHORT_SHA
    CONTAINER: sensor-$SENSOR_ID
  script:
    - chmod 600 $ID_RSA
    - apk update && apk add openssh-client
    - ssh -i $ID_RSA -o StrictHostKeyChecking=no $SERVER_USER@$SERVER_IP "docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY"
    - ssh -i $ID_RSA -o StrictHostKeyChecking=no $SERVER_USER@$SERVER_IP "docker pull $TAG_COMMIT"
    - ssh -i $ID_RSA -o StrictHostKeyChecking=no $SERVER_USER@$SERVER_IP "docker container rm -f $CONTAINER || true"
    - >-
      ssh -i $ID_RSA -o StrictHostKeyChecking=no $SERVER_USER@$SERVER_IP "docker run -d -e AMQP_URL=$QUEUE_URL 
      -e QUEUE_NAME=$QUEUE_NAME -e SENSOR_ID=$SENSOR_ID -e SENSOR_DELAY=$SENSOR_DELAY 
      --name $CONTAINER $TAG_COMMIT"
  when: manual
  only:
    - main


#grpc-coverage-report:
#  stage: all
#  needs: [ grpc-build ]
#  image: registry.gitlab.com/haynes/jacoco2cobertura:1.0.7
#  script:
#    # convert report from jacoco to cobertura, using relative project path
#    - python /opt/cover2cover.py build/reports/jacoco/test/jacocoTestReport.xml $CI_PROJECT_DIR/src/main/kotlin/ > build/cobertura.xml
#    - cat build/reports/jacoco/test/html/index.html | grep -o '.*'
#  coverage: "/Total.*?([0-9]{1,3})%/"
#  artifacts:
#    reports:
#      cobertura: build/cobertura.xml

#format-and-check-style:
#  stage: code-health
#  script:
#    - ./gradlew ktlintFormat && ./gradlew ktlintFormat ktlintCheck

#build-jar:
#  stage: build
#  script:
#    - ./gradlew assemble
#  artifacts:
#    paths:
#      - build/libs/*.jar
#    expire_in: 1 week
#  only:
#    - main

#build-container:
#  image: docker:latest
#  services:
#    - docker:dind
#  stage: build
#  needs: [ 'lint-build-test' ]
#  script:
#    - docker login -u $CI_DEPLOY_USER -p $CI_DEPLOY_PASSWORD $CI_REGISTRY
#    - docker pull $CI_REGISTRY_IMAGE:latest || true
#    - docker build --cache-from $CI_REGISTRY_IMAGE:latest --tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA --tag $CI_REGISTRY_IMAGE:latest --build-arg DB_IP=$DB_IP --build-arg DB_PORT=$DB_PORT --build-arg DB_USER=$DB_USER --build-arg DB_PASSWORD=$DB_PASSWORD --build-arg DB_DBNAME=$DB_DBNAME --build-arg PROD_SERVER=PROD_SERVER  --build-arg AMQP_URL=$AMQP_URL  --build-arg QUEUE_NAME=$QUEUE_NAME  --build-arg JWT_KEY=$JWT_KEY   .
#    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
#    - docker push $CI_REGISTRY_IMAGE:latest
#  only:
#    - main
#
#deploy-container:
#  stage: deploy
#  needs: [ 'build-container' ]
#  image: docker:latest
#  services:
#    - docker:dind
#  only:
#    - main
#  script:
#    - docker login -u $CI_DEPLOY_USER -p $CI_DEPLOY_PASSWORD $CI_REGISTRY
#    - docker pull $CI_REGISTRY_IMAGE:latest
#    - docker tag $CI_REGISTRY_IMAGE:latest $HEROKU_REGISTRY_IMAGE:latest
#    - docker login --username=_ --password=$HEROKU_API_KEY $HEROKU_REGISTRY
#    - docker push $HEROKU_REGISTRY_IMAGE:latest
#    - docker run --rm -e HEROKU_API_KEY=$HEROKU_API_KEY wingrunr21/alpine-heroku-cli container:release web --app $APP_NAME
#




# generate-openapi-docs:
#     stage: build
#     script:
#         - ./gradlew generateOpenApiDocs
#     artifacts:
#         paths:
#             - build/openapi.json
#         expire_in: 1 week

#generate-angular-client:
#  stage: generate-client
#  script:
#    - ./gradlew openApiGenerate
#  artifacts:
#    paths:
#      - build/openapi.json
#      - build/typescript-angular
#    expire_in: 1 min
#  rules:
#    - when: manual
#    - changes:
#        - src/main/kotlin/com/katonaaron/energy/exposition/**
#        - src/main/kotlin/com/katonaaron/energy/application/**/dto/*



#deploy-angular-client-package:
#  stage: deploy
#  image: node:11
#  needs: [ "generate-angular-client" ]
#  dependencies:
#    - generate-angular-client
#  script:
#    - cd build/typescript-angular
#    - echo "@energy-platform:registry=https://${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}/packages/npm/" >.npmrc
#    - echo "//${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}/packages/npm/:_authToken=${CI_JOB_TOKEN}" >> .npmrc
#    - npm install
#    - npm run build
#    - npm publish dist
#  rules:
#    - when: manual
##    - changes:
##        - src/main/kotlin/com/katonaaron/energy/exposition/**
##        - src/main/kotlin/com/katonaaron/energy/application/**/dto/*
