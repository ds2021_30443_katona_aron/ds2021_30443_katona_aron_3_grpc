/*
 * Copyright (c) 2021 Áron Katona
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.katonaaron.energy.application.clients

import com.katonaaron.energy.application.ApplicationService
import com.katonaaron.energy.application.clients.dto.ClientDto
import com.katonaaron.energy.application.clients.dto.CreateClientDto
import com.katonaaron.energy.application.clients.dto.toDto
import com.katonaaron.energy.application.useraccount.CreateUserAccount
import com.katonaaron.energy.domain.DDD
import com.katonaaron.energy.domain.client.Client
import com.katonaaron.energy.domain.client.Clients
import com.katonaaron.energy.domain.useraccount.UserRole

@DDD.ApplicationService
@ApplicationService
class CreateClient(
    private val clients: Clients,
    private val createUserAccount: CreateUserAccount
) {

    fun createClient(dto: CreateClientDto): ClientDto {
        val account = createUserAccount.createUserAccount(
            username = dto.username,
            password = dto.password,
            role = UserRole.ROLE_CLIENT
        )

        val id = clients.nextIdentity()
        val client = Client(
            id,
            dto.name,
            dto.birthDate,
            dto.address,
            account
        )
        val savedClient = clients.save(client)

        return savedClient.toDto()
    }
}
