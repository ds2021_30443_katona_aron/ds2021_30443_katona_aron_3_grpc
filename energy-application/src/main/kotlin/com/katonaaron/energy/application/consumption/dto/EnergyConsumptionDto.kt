/*
 * Copyright (c) 2021-2022 Áron Katona
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.katonaaron.energy.application.consumption.dto

import com.fasterxml.jackson.annotation.JsonFormat
import com.katonaaron.energy.domain.device.EnergyConsumption
import com.katonaaron.energy.domain.energy.Energy
import com.katonaaron.energy.domain.energy.EnergyUnit
import io.swagger.v3.oas.annotations.media.Schema
import java.time.Instant

@Schema(name = "EnergyConsumption")
data class EnergyConsumptionDto(
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss", timezone = "UTC")
    val timestamp: Instant,
    val consumption: Double
)

fun EnergyConsumption.toDto(): EnergyConsumptionDto =
    EnergyConsumptionDto(
        timestamp = timestamp,
        consumption = value.toDto()
    )

fun Energy.toDto(): Double = amount(EnergyUnit.kWh)
