/*
 * Copyright (c) 2021 Áron Katona
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.katonaaron.energy.application.sensors

import com.katonaaron.energy.application.ApplicationService
import com.katonaaron.energy.application.exceptions.ResourceNotFoundException
import com.katonaaron.energy.application.sensors.dto.SensorDto
import com.katonaaron.energy.application.sensors.dto.UpdateSensorDto
import com.katonaaron.energy.application.sensors.dto.toDto
import com.katonaaron.energy.domain.DDD
import com.katonaaron.energy.domain.energy.Energy
import com.katonaaron.energy.domain.energy.EnergyUnit
import com.katonaaron.energy.domain.sensor.Sensor
import com.katonaaron.energy.domain.sensor.SensorId
import com.katonaaron.energy.domain.sensor.Sensors

@DDD.ApplicationService
@ApplicationService
class UpdateSensor(
    private val sensors: Sensors
) {

    fun updateSensor(id: SensorId, dto: UpdateSensorDto): SensorDto {
        val sensor = sensors.findById(id)
            ?: throw ResourceNotFoundException(Sensor::class.simpleName + " with id: " + id)

        dto.description?.let { sensor.description = it }
        dto.maxValue?.let { sensor.maxValue = Energy(it, EnergyUnit.kWh) }

        return sensors.save(sensor).toDto()
    }
}
