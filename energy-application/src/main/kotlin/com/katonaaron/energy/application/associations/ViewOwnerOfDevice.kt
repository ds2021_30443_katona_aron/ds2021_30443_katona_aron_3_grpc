/*
 * Copyright (c) 2021 Áron Katona
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.katonaaron.energy.application.associations

import com.katonaaron.energy.application.ApplicationService
import com.katonaaron.energy.application.clients.dto.ClientDto
import com.katonaaron.energy.application.clients.dto.toDto
import com.katonaaron.energy.application.exceptions.ResourceNotFoundException
import com.katonaaron.energy.domain.DDD
import com.katonaaron.energy.domain.device.Device
import com.katonaaron.energy.domain.device.DeviceId
import com.katonaaron.energy.domain.device.Devices

@DDD.ApplicationService
@ApplicationService
class ViewOwnerOfDevice(
    private val devices: Devices
) {
    fun viewOwnerOfDevice(deviceId: DeviceId): ClientDto? {
        val device = devices.findById(deviceId)
            ?: throw ResourceNotFoundException("${Device::class.simpleName} with id $deviceId")
        return device.owner?.toDto()
    }
}
