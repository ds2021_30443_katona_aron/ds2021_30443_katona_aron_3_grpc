/*
 * Copyright (c) 2021 Áron Katona
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.katonaaron.energy.application.clients.dto

import com.fasterxml.jackson.annotation.JsonFormat
import com.katonaaron.energy.domain.client.Client
import io.swagger.v3.oas.annotations.media.Schema
import java.time.LocalDate

@Schema(name = "Client")
class ClientDto(
    val id: String,
    val username: String,
    val name: String,
    @JsonFormat(pattern = "yyyy-MM-dd")
    val birthDate: LocalDate,
    val address: String
)

fun Client.toDto() =
    ClientDto(
        id = id.value,
        name = name,
        birthDate = birthDate,
        address = address,
        username = account.username
    )
