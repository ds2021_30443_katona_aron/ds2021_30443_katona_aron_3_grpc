/*
 * Copyright (c) 2021 Áron Katona
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.katonaaron.energy.application.devices

import com.katonaaron.energy.application.ApplicationService
import com.katonaaron.energy.application.devices.dto.DeviceDto
import com.katonaaron.energy.application.devices.dto.UpdateDeviceDto
import com.katonaaron.energy.application.devices.dto.toDto
import com.katonaaron.energy.application.exceptions.ResourceNotFoundException
import com.katonaaron.energy.domain.DDD
import com.katonaaron.energy.domain.device.Device
import com.katonaaron.energy.domain.device.DeviceId
import com.katonaaron.energy.domain.device.Devices
import com.katonaaron.energy.domain.energy.Energy
import com.katonaaron.energy.domain.energy.EnergyUnit

@DDD.ApplicationService
@ApplicationService
class UpdateDevice(
    private val devices: Devices
) {

    fun updateDevice(id: DeviceId, dto: UpdateDeviceDto): DeviceDto {
        val device = devices.findById(id)
            ?: throw ResourceNotFoundException(Device::class.simpleName + " with id: " + id)

        dto.address?.let { device.address = it }
        dto.description?.let { device.description = it }
        dto.maxEnergyConsumption?.let { device.maxEnergyConsumption = Energy(it, EnergyUnit.kWh) }
        dto.baselineEnergyConsumption?.let { device.baselineEnergyConsumption = Energy(it, EnergyUnit.kWh) }

        return devices.save(device).toDto()
    }
}
