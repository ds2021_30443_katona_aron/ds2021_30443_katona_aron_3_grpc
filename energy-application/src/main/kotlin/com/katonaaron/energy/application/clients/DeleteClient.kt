/*
 * Copyright (c) 2021 Áron Katona
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.katonaaron.energy.application.clients

import com.katonaaron.energy.application.ApplicationService
import com.katonaaron.energy.domain.DDD
import com.katonaaron.energy.domain.client.ClientId
import com.katonaaron.energy.domain.client.Clients

@DDD.ApplicationService
@ApplicationService
class DeleteClient(
    private val clients: Clients
) {

    fun deleteClient(id: ClientId) {
        val client = clients.findById(id) ?: return

        if (client.hasAnyDevice()) {
            client.removeAllDevices()
        }

        clients.delete(client)
    }
}
