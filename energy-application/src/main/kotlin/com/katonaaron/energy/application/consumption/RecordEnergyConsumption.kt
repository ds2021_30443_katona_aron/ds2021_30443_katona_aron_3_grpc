/*
 * Copyright (c) 2021 Áron Katona
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.katonaaron.energy.application.consumption

import com.katonaaron.energy.application.ApplicationService
import com.katonaaron.energy.application.exceptions.ResourceNotFoundException
import com.katonaaron.energy.domain.DDD
import com.katonaaron.energy.domain.device.EnergyConsumption
import com.katonaaron.energy.domain.energy.Energy
import com.katonaaron.energy.domain.energy.EnergyUnit
import com.katonaaron.energy.domain.sensor.Sensor
import com.katonaaron.energy.domain.sensor.SensorId
import com.katonaaron.energy.domain.sensor.Sensors
import java.time.Instant

@DDD.ApplicationService
@ApplicationService
class RecordEnergyConsumption(
    private val sensors: Sensors
) {
    fun recordEnergyConsumption(sensorId: SensorId, value: Double) {
        val sensor = sensors.findById(sensorId)
            ?: throw ResourceNotFoundException(Sensor::class.simpleName + " with id: " + sensorId)

        sensor.recordEnergyConsumption(Energy(value, EnergyUnit.kWh))
        sensors.save(sensor)
    }

    fun recordEnergyConsumption(sensorId: SensorId, timestamp: Instant, value: Double) {
        val sensor = sensors.findById(sensorId)
            ?: throw ResourceNotFoundException(Sensor::class.simpleName + " with id: " + sensorId)

        sensor.recordEnergyConsumption(EnergyConsumption(timestamp, Energy(value, EnergyUnit.kWh)))
        sensors.save(sensor)
    }
}
