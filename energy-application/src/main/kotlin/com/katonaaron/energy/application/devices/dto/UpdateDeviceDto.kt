/*
 * Copyright (c) 2021 Áron Katona
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.katonaaron.energy.application.devices.dto

import io.swagger.v3.oas.annotations.media.Schema
import javax.validation.constraints.Size

data class UpdateDeviceDto(
    @field:Size(min = 3, max = 255, message = "The description must be at least 3, at most 255 characters long")
    @Schema(example = "Smart heater")
    val description: String?,
    @Schema(example = "John str. 21")
    val address: String?,
    @Schema(example = "1230", description = "Maximum possible energy consumption in kWh")
    val maxEnergyConsumption: Double?,
    @Schema(example = "523", description = "Baseline energy consumption in kWh")
    val baselineEnergyConsumption: Double?
)
