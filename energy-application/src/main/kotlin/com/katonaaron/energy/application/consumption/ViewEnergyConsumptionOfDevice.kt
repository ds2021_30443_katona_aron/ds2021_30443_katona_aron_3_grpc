/*
 * Copyright (c) 2021 Áron Katona
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.katonaaron.energy.application.consumption

import com.katonaaron.energy.application.ApplicationService
import com.katonaaron.energy.application.consumption.dto.EnergyConsumptionDto
import com.katonaaron.energy.application.consumption.dto.toDto
import com.katonaaron.energy.application.exceptions.ResourceNotFoundException
import com.katonaaron.energy.domain.DDD
import com.katonaaron.energy.domain.device.Device
import com.katonaaron.energy.domain.device.DeviceId
import com.katonaaron.energy.domain.device.Devices
import com.katonaaron.energy.util.toLocalDate
import java.time.LocalDate

@DDD.ApplicationService
@ApplicationService
class ViewEnergyConsumptionOfDevice(
    private val devices: Devices
) {
    fun viewCurrentEnergyConsumptionOfDevice(deviceId: DeviceId): EnergyConsumptionDto? =
        findDevice(deviceId).currentEnergyConsumption?.toDto()

    fun viewHistoricalEnergyConsumptionOfDevice(deviceId: DeviceId): Collection<EnergyConsumptionDto> =
        findDevice(deviceId).energyConsumptionHistory.map { it.toDto() }

    fun viewHistoricalEnergyConsumptionOfDevice(deviceId: DeviceId, day: LocalDate): Collection<EnergyConsumptionDto> =
        findDevice(deviceId).energyConsumptionHistory
            .filter { it.timestamp.toLocalDate() == day }
            .map { it.toDto() }

    private fun findDevice(deviceId: DeviceId): Device = devices.findById(deviceId)
        ?: throw ResourceNotFoundException(Device::class.simpleName + " with id: " + deviceId)
}
