/*
 * Copyright (c) 2021-2022 Áron Katona
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.katonaaron.energy.application.consumption

import com.katonaaron.energy.application.ApplicationService
import com.katonaaron.energy.application.consumption.dto.EnergyConsumptionDto
import com.katonaaron.energy.application.consumption.dto.toDto
import com.katonaaron.energy.application.exceptions.ResourceNotFoundException
import com.katonaaron.energy.domain.DDD
import com.katonaaron.energy.domain.client.Client
import com.katonaaron.energy.domain.client.ClientId
import com.katonaaron.energy.domain.client.Clients
import com.katonaaron.energy.domain.energy.Energy
import com.katonaaron.energy.domain.energy.EnergyUnit

@DDD.ApplicationService
@ApplicationService
class ViewEnergyConsumptionOfClient(
    private val clients: Clients
) {

    fun viewTotalEnergyConsumptionOfClient(clientId: ClientId): Double? {
        val client = findClient(clientId)
        return client.devices
            .mapNotNull { it.currentEnergyConsumption?.value }
            .reduceOrNull(Energy::plus)
            ?.amount(EnergyUnit.kWh)
    }

    fun viewHourlyEnergyConsumptionOfClient(clientId: ClientId, nrPastDays: Long): Collection<EnergyConsumptionDto> =
        findClient(clientId).getHourlyEnergyConsumption(nrPastDays)
            .map { it.toDto() }

    fun viewBaselineEnergyConsumptionOfClient(clientId: ClientId): Array<Double> =
        findClient(clientId).getBaselineEnergyConsumption()
            .map { it.toDto() }
            .toTypedArray()

    private fun findClient(clientId: ClientId): Client = clients.findById(clientId)
        ?: throw ResourceNotFoundException(Client::class.simpleName + " with id: " + clientId)
}
