/*
 * Copyright (c) 2021-2022 Áron Katona
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.katonaaron.energy.application.devices.dto

import com.katonaaron.energy.domain.device.Device
import com.katonaaron.energy.domain.energy.EnergyUnit
import io.swagger.v3.oas.annotations.media.Schema

@Schema(name = "Device")
data class DeviceDto(
    val id: String,
    val description: String,
    val address: String,
    val maxEnergyConsumption: Double,
    val baselineEnergyConsumption: Double,
    val maxRelativeEnergyConsumption: Double?,
    val currentEnergyConsumption: Double?,
    val currentPowerPeak: Double?
)

fun Device.toDto(): DeviceDto = DeviceDto(
    id = id.value,
    address = address,
    description = description,
    maxEnergyConsumption = maxEnergyConsumption.amount(EnergyUnit.kWh),
    baselineEnergyConsumption = baselineEnergyConsumption.amount(EnergyUnit.kWh),
    maxRelativeEnergyConsumption = maxRelativeEnergyConsumption?.value?.amount(EnergyUnit.kWh),
    currentEnergyConsumption = currentEnergyConsumption?.value?.amount(EnergyUnit.kWh),
    currentPowerPeak = powerPeak?.amount(EnergyUnit.kWh),
)
