/*
 * Copyright (c) 2022 Áron Katona
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.katonaaron.energy.application.scheduling

import com.katonaaron.energy.application.ApplicationService
import com.katonaaron.energy.application.consumption.dto.toDto
import com.katonaaron.energy.application.exceptions.ResourceNotFoundException
import com.katonaaron.energy.application.scheduling.dto.ScheduleDto
import com.katonaaron.energy.domain.DDD
import com.katonaaron.energy.domain.DomainConstraintValidationException
import com.katonaaron.energy.domain.client.Client
import com.katonaaron.energy.domain.client.ClientId
import com.katonaaron.energy.domain.client.Clients
import com.katonaaron.energy.domain.device.Device
import com.katonaaron.energy.domain.device.DeviceId
import com.katonaaron.energy.domain.device.Devices
import com.katonaaron.energy.domain.energy.amountSI
import java.time.LocalDate

@DDD.ApplicationService
@ApplicationService
class ScheduleDevice(
    private val devices: Devices,
    private val clients: Clients
) {
    fun scheduleDevice(clientId: ClientId, deviceId: DeviceId, numberOfHours: Int): ScheduleDto? {
        val client = findClient(clientId)
        val device = findDevice(deviceId)

        if (!client.hasDevice(device)) {
            throw DomainConstraintValidationException("Device \"$deviceId\" is not assigned to client \"$clientId\" ")
        }

        val baseline = client.getBaselineEnergyConsumption()
        val duration = numberOfHours.mod(24)

        var minSum = 0.0
        var minInd = -1

        assert(baseline.size == 24)

        for (i in 0 until 24) {

            var sum = 0.0

            for (offset in 0 until duration) {
                sum += baseline[(i + offset).mod(24)].amountSI()
            }

            if (minInd == -1 || sum < minSum) {
                minSum = sum
                minInd = i
            }
        }

        val startTime = LocalDate.now().plusDays(1).atStartOfDay().plusHours(minInd.toLong())
        val estimatedConsumption = baseline.copyOf()
        val maxDeviceConsumption = device.maxRelativeEnergyConsumption?.value
            ?: return null

        val updateInterval = if (numberOfHours >= 24) 24 else duration
        for (offset in 0 until updateInterval) {
            estimatedConsumption[(minInd + offset).mod(24)] += maxDeviceConsumption
        }

        return ScheduleDto(startTime, estimatedConsumption.map { it.toDto() })
    }

    private fun findClient(clientId: ClientId): Client = clients.findById(clientId)
        ?: throw ResourceNotFoundException(Client::class.simpleName + " with id: " + clientId)

    private fun findDevice(deviceId: DeviceId): Device = devices.findById(deviceId)
        ?: throw ResourceNotFoundException(Device::class.simpleName + " with id: " + deviceId)
}
