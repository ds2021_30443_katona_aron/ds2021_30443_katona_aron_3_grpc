/*
 * Copyright (c) 2021 Áron Katona
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.katonaaron.energy.application.clients.dto

import io.swagger.v3.oas.annotations.media.Schema
import java.time.LocalDate
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Past
import javax.validation.constraints.Size

data class CreateClientDto(
    @field:Size(min = 3, message = "The username must contain at least 3 characters")
    @field:NotBlank(message = "Must provide a username")
    @Schema(example = "user1")
    val username: String,

    @field:Size(min = 8, message = "The password must contain at least 8 characters")
    @Schema(example = "pass1234")
    val password: String,

    @field:Size(min = 3, max = 255, message = "The name must be at least 3, at most 255 characters long")
    @field:NotBlank(message = "Must provide a name")
    @Schema(example = "John Doe")
    val name: String,
    @field:Past
    @Schema(example = "2021-10-30")
    val birthDate: LocalDate,
    @field:NotBlank(message = "Must provide an address")
    @Schema(example = "John str. 21")
    val address: String
)
