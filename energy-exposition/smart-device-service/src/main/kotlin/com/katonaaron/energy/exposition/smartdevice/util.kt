/*
 * Copyright (c) 2021-2022 Áron Katona
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.katonaaron.energy.exposition.smartdevice

import com.google.protobuf.Timestamp
import com.katonaaron.energy.application.consumption.dto.EnergyConsumptionDto
import com.katonaaron.energy.application.devices.dto.DeviceDto
import com.katonaaron.energy.exposition.dataplatform.Device
import com.katonaaron.energy.exposition.dataplatform.device
import com.katonaaron.energy.util.toInstant
import java.time.Instant
import java.time.LocalDateTime

fun Instant.toTimestamp(): Timestamp = Timestamp.newBuilder().setSeconds(epochSecond).setNanos(nano).build()

fun LocalDateTime.toTimestamp(): Timestamp = toInstant().toTimestamp()

fun EnergyConsumptionDto.toMessage(): EnergyConsumption = energyConsumption {
    this.consumption = this@toMessage.consumption
    this.timestamp = this@toMessage.timestamp.toTimestamp()
}

fun DeviceDto.toMessage(): Device = device {
    id = this@toMessage.id
    description = this@toMessage.description
    address = this@toMessage.address
    maxEnergyConsumption = this@toMessage.maxEnergyConsumption
    baselineEnergyConsumption = this@toMessage.baselineEnergyConsumption
    this@toMessage.maxRelativeEnergyConsumption?.let { maxRelativeEnergyConsumption = it }
    this@toMessage.currentEnergyConsumption?.let { currentEnergyConsumption = it }
    this@toMessage.currentPowerPeak?.let { currentPowerPeak = it }
}
