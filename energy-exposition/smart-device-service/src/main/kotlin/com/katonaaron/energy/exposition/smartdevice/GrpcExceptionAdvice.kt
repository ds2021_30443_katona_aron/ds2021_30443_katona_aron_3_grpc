/*
 * Copyright (c) 2021 Áron Katona
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.katonaaron.energy.exposition.smartdevice

import com.katonaaron.energy.application.exceptions.ResourceNotFoundException
import com.katonaaron.energy.domain.DomainConstraintValidationException
import io.grpc.Status
import net.devh.boot.grpc.server.advice.GrpcAdvice
import net.devh.boot.grpc.server.advice.GrpcExceptionHandler

@GrpcAdvice
class GrpcExceptionAdvice {
    @GrpcExceptionHandler
    fun handleInvalidArgument(e: IllegalArgumentException): Status {
        return Status.INVALID_ARGUMENT.withDescription(e.message).withCause(e)
    }

    @GrpcExceptionHandler(ResourceNotFoundException::class)
    fun handleResourceNotFoundException(e: ResourceNotFoundException): Status {
        return Status.NOT_FOUND.withDescription(e.message).withCause(e)
    }

    @GrpcExceptionHandler(DomainConstraintValidationException::class)
    fun handleException(e: DomainConstraintValidationException): Status {
        return Status.FAILED_PRECONDITION.withDescription(e.message).withCause(e)
    }
}
