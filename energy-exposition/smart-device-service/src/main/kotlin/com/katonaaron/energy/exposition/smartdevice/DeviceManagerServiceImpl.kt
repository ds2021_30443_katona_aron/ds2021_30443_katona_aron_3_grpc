/*
 * Copyright (c) 2022 Áron Katona
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.katonaaron.energy.exposition.smartdevice

import com.katonaaron.energy.application.associations.ViewDevicesOfClient
import com.katonaaron.energy.domain.client.ClientId
import com.katonaaron.energy.exposition.dataplatform.DeviceManagerGrpcKt
import com.katonaaron.energy.exposition.dataplatform.GetDeviceOfClientReply
import com.katonaaron.energy.exposition.dataplatform.GetDeviceOfClientRequest
import com.katonaaron.energy.exposition.dataplatform.getDeviceOfClientReply
import net.devh.boot.grpc.server.service.GrpcService

@GrpcService
class DeviceManagerServiceImpl(
    private val viewDevicesOfClient: ViewDevicesOfClient
) : DeviceManagerGrpcKt.DeviceManagerCoroutineImplBase() {

    override suspend fun getDevicesOfClient(request: GetDeviceOfClientRequest): GetDeviceOfClientReply {
        val devices = viewDevicesOfClient.viewDevicesOfClient(ClientId(request.clientId))
            .map { it.toMessage() }
        return getDeviceOfClientReply {
            this.devices.addAll(devices)
        }
    }
}
