/*
 * Copyright (c) 2021-2022 Áron Katona
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.katonaaron.energy.exposition.smartdevice

import com.katonaaron.energy.application.consumption.ViewEnergyConsumptionOfClient
import com.katonaaron.energy.application.scheduling.ScheduleDevice
import com.katonaaron.energy.domain.client.ClientId
import com.katonaaron.energy.domain.device.DeviceId
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.map
import net.devh.boot.grpc.server.service.GrpcService

@GrpcService
class SmartDeviceServiceImpl(
    private val viewEnergyConsumptionOfClient: ViewEnergyConsumptionOfClient,
    private val scheduleDevice: ScheduleDevice
) : SmartDeviceGrpcKt.SmartDeviceCoroutineImplBase() {

    override fun getHistoricalEnergyConsumption(request: GetHistoricalEnergyConsumptionRequest): Flow<EnergyConsumption> =
        viewEnergyConsumptionOfClient.viewHourlyEnergyConsumptionOfClient(
            ClientId(request.clientId),
            request.numberOfDays.toLong()
        ).asFlow().map { it.toMessage() }

    override suspend fun getBaselineEnergyConsumption(request: GetBaselineEnergyConsumptionRequest): ConsumptionList {
        val baseline = viewEnergyConsumptionOfClient.viewBaselineEnergyConsumptionOfClient(
            ClientId(request.clientId),
        )

        return ConsumptionList.newBuilder()
            .addAllBaseline(baseline.toList())
            .build()
    }

    override suspend fun scheduleDevice(request: ScheduleDeviceRequest): ScheduleDeviceReply {
        val schedule =
            scheduleDevice.scheduleDevice(ClientId(request.clientId), DeviceId(request.deviceId), request.duration)
        val baseline = viewEnergyConsumptionOfClient.viewBaselineEnergyConsumptionOfClient(
            ClientId(request.clientId),
        )
        return scheduleDeviceReply {
            schedule?.let {
                startTime = it.startTime.toTimestamp()
                estimatedConsumption.addAll(it.estimatedConsumption)
            }
            this.baseline.addAll(baseline.toList())
        }
    }

    //        return viewAllDevices.viewAllDevices()
//            .firstOrNull()?.let { device ->
//                viewEnergyConsumptionOfDevice.viewHistoricalEnergyConsumptionOfDevice(DeviceId(device.id))
//            }?.map {
//                energyConsumption {
//                    this.consumption = it.consumption
//                    this.timestamp =
//                        Timestamp.newBuilder().setSeconds(it.timestamp.epochSecond).setNanos(it.timestamp.nano).build()
//                }
//            }?.asFlow()
//            ?: flowOf()
//    {
//        println(request)
//        return flowOf(energyConsumption {
//            timestamp = Timestamp.getDefaultInstance()
//            consumption = 123.45
//        })
//    }
}
