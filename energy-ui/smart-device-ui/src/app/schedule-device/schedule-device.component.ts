/*
 * Copyright (c) 2022 Áron Katona
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import {Component, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subscription} from "rxjs";
import {SmartDeviceClient} from "../../proto/com/katonaaron/energy/exposition/smartdevice/smart-device.pbsc";
import {ScheduleDeviceRequest} from "../../proto/com/katonaaron/energy/exposition/smartdevice/smart-device.pb";
import {faSpinner} from '@fortawesome/free-solid-svg-icons';
import {getTimeZoneOffset} from "../shared/util";
import {StoreService} from "../shared/store/store.service";
import {Device} from "../../proto/com/katonaaron/energy/exposition/dataplatform/device-manager.pb";

@Component({
  selector: 'app-schedule-device',
  templateUrl: './schedule-device.component.html',
  styleUrls: ['./schedule-device.component.css']
})
export class ScheduleDeviceComponent implements OnInit, OnDestroy {
  chartData: any[] = [];
  xAxisTicks = [...Array(24).keys()]
  xAxisLabel: string = 'Time';
  yAxisLabel: string = 'Energy [kW]';

  faSpinner = faSpinner;

  loading = false;

  clientId: string | null = null;
  deviceId: string | null = null;
  duration = 7;
  startTime: Date | null = null
  devices$!: Observable<Device[]>;

  subscription: Subscription | null = null;

  constructor(private client: SmartDeviceClient,
              private storeService: StoreService) {
  }

  get showChart(): boolean {
    return this.chartData.length > 0 && this.chartData[0].series.length > 0;
  }

  ngOnDestroy(): void {
    this.clearData();
  }

  ngOnInit(): void {
    this.storeService.clientId$.subscribe(
      clientId => {
        this.clientId = clientId;
        this.updateData(clientId, this.deviceId, this.duration);
      }
    )

    this.devices$ = this.storeService.devices$;
  }

  onDurationChange(duration: number) {
    this.updateData(this.clientId, this.deviceId, duration);
  }

  onSelectedDeviceChange(selectedDeviceId: string) {
    this.updateData(this.clientId, selectedDeviceId, this.duration);
  }

  private fetchData(clientId: string, deviceId: string, duration: number): void {
    this.loading = true;

    if (this.subscription) {
      this.subscription.unsubscribe();
      this.subscription = null;
    }

    const request = new ScheduleDeviceRequest({
      clientId: clientId,
      deviceId: deviceId,
      duration: duration
    });

    this.subscription = this.client.scheduleDevice(request)
      .subscribe(
        reply => {
          const timeOffset = getTimeZoneOffset();
          this.chartData = [
            {
              name: "Estimated",
              series: reply.estimatedConsumption?.map((consumption, index) => ({
                  name: (index + timeOffset) % 24,
                  value: consumption
                }) || []
              )
            },
            {
              name: "Baseline",
              series: reply.baseline!.map((consumption, index) => ({
                  name: (index + timeOffset) % 24,
                  value: consumption
                })
              )
            }
          ];
          this.startTime = reply.startTime?.toDate() || null
          console.log("success", reply);
        },
        err => {
          console.log("error", err);
        },
        () => {
          this.loading = false;
          console.log("complete");
        }
      )
  }

  private clearData() {
    if (this.subscription) {
      this.subscription.unsubscribe();
      this.subscription = null;
    }
    this.chartData = [];
    this.startTime = null
  }

  private updateData(clientId: string | null, deviceId: string | null, duration: number) {
    if (!!clientId && !!deviceId && duration >= 1) {
      this.fetchData(clientId, deviceId, duration);
    } else {
      this.clearData();
    }
  }
}
