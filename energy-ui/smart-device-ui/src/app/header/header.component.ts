/*
 * Copyright (c) 2022 Áron Katona
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {faUser} from '@fortawesome/free-solid-svg-icons';
import {environment} from "../../environments/environment";
// import {AuthService} from "../auth/auth.service";
import {Location} from '@angular/common';
import {AuthService} from "../shared/auth/auth.service";
import {User, UserRole} from '../shared/auth/user.model';

// import {User, UserRole} from "../auth/user.model";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  UserRole = UserRole;
  appTitle = environment.appTitle;
  collapsed = true;
  faUser = faUser;
  isLoggedIn = false;
  user: User | null = null;

  constructor(private router: Router,
              private location: Location,
              private authService: AuthService) {
  }

  ngOnInit(): void {
    this.authService.currentUser$.subscribe(user => {
      this.isLoggedIn = user != null;
      this.user = user;
    });
  }

  onLogout(): void {
    this.authService.logout();
    this.router.navigate(['/']);
  }

  onLogin(): void {
    this.router.navigate(['login'], {queryParams: {redirectURL: this.router.url}});
  }

}
