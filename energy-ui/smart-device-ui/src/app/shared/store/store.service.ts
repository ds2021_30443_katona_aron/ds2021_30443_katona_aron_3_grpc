/*
 * Copyright (c) 2022 Áron Katona
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import {Injectable} from '@angular/core';
import {Observable, of} from "rxjs";
import {
  Device,
  GetDeviceOfClientRequest
} from "../../../proto/com/katonaaron/energy/exposition/dataplatform/device-manager.pb";
import {map, mergeMap} from "rxjs/operators";
import {DeviceManagerClient} from "../../../proto/com/katonaaron/energy/exposition/dataplatform/device-manager.pbsc";
import {AuthService} from "../auth/auth.service";
import {User} from "../auth/user.model";

@Injectable({
  providedIn: 'root'
})
export class StoreService {
  public readonly user$: Observable<User | null>;
  public readonly clientId$: Observable<string | null>;
  public readonly devices$: Observable<Device[]>;

  constructor(private deviceManagerClient: DeviceManagerClient,
              private authService: AuthService) {
    this.user$ = this.authService.currentUser$;
    this.clientId$ = this.user$.pipe(
      map(user => user?.clientId || null)
    );

    this.devices$ = this.clientId$.pipe(
      mergeMap(clientId => {
        if (!clientId)
          return of([]);
        return this.deviceManagerClient.getDevicesOfClient(new GetDeviceOfClientRequest({
          clientId: clientId
        })).pipe(map(reply => reply.devices || []))
      })
    );
  }


}
