/*
 * Copyright (c) 2022 Áron Katona
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import {Component, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';

import {first} from 'rxjs/operators';
import {AuthService} from "../auth.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  submitted = false;
  loading = false;
  redirectURL = '/';
  error = '';

  constructor(private fb: FormBuilder,
              private authService: AuthService,
              private router: Router,
              private route: ActivatedRoute) {
    this.form = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });

    if (authService.isAuthenticated()) {
      this.router.navigate(['/']);
    }
  }

  // convenience getter for easy access to form fields
  get f(): { [p: string]: AbstractControl } {
    return this.form.controls;
  }

  get fUsername(): AbstractControl {
    return this.f['username'];
  }

  get fPassword(): AbstractControl {
    return this.f['password'];
  }

  ngOnInit(): void {
    this.route.queryParams
      .subscribe((params) => {
        this.redirectURL = params['redirectURL'] || '/';
      });
  }

  onSubmit(): void {
    this.submitted = true;

    // stop here if form is invalid
    if (this.form.invalid) {
      return;
    }

    this.loading = true;

    const val = this.form.value;

    if (val.username && val.password) {
      this.authService.login(val.username, val.password)
        .pipe(first())
        .subscribe(
          data => {
            this.router.navigateByUrl(this.redirectURL, {replaceUrl: true});
            this.loading = false;
          },
          error => {
            this.error = 'Invalid credentials';
            this.loading = false;
          }
        );
    }
  }

}
