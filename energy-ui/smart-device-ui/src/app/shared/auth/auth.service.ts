/*
 * Copyright (c) 2022 Áron Katona
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {JwtHelperService} from '@auth0/angular-jwt';
import {TokenService} from './token.service';
import {BehaviorSubject, Observable, throwError} from 'rxjs';
import {User, UserRole} from './user.model';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public readonly currentUser$: Observable<User | null>;
  private readonly urlLogin = environment.authServerUrl + '/login';
  private readonly currentUserSubject: BehaviorSubject<User | null>;

  constructor(private http: HttpClient,
              private jwtHelper: JwtHelperService,
              private tokenService: TokenService) {
    this.currentUserSubject = new BehaviorSubject<User | null>(this.tokenToUser(this.getToken()));
    this.currentUser$ = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User | null {
    return this.currentUserSubject.value;
  }

  public isAuthenticated(): boolean {
    return !!this.getToken();
  }

  login(username: string, password: string): Observable<User> {
    return this.http.post<any>(this.urlLogin, {username, password}, {observe: 'response'})
      .pipe(
        map(resp => {
          const token = resp.headers.get('X-Token');
          console.log(token);
          if (token == null) {
            throw throwError("Invalid token")
          }

          const user = this.tokenToUser(token);
          console.log(user);

          if (user == null) {
            throw throwError("Invalid token")
          }

          this.tokenService.setToken(token);
          this.currentUserSubject.next(user);
          return user;
        }),
        // shareReplay(1)
      );
    // this is just the HTTP call,
    // we still need to handle the reception of the token
    // .shareReplay();
  }

  logout(): void {
    this.tokenService.clearToken();
    if (this.currentUserSubject) {
      this.currentUserSubject.next(null);
    }
  }

  private tokenToUser(token: string | null): User | null {
    if (!token) {
      return null;
    }

    const decodedToken = this.jwtHelper.decodeToken(token);

    console.log(decodedToken)
    if (decodedToken == null) {
      return null;
    }

    const user = new User();
    switch (decodedToken.role) {
      case 'ROLE_CLIENT':
        user.role = UserRole.ROLE_CLIENT;
        user.clientId = decodedToken.clientId;
        break;
      case 'ROLE_ADMIN':
        user.role = UserRole.ROLE_ADMIN;
        user.clientId = null;
        break;
      default:
        return null;
    }
    user.username = decodedToken.sub;
    return user;
  }

  private getToken(): string | null {
    const token = this.tokenService.getToken();
    if (!token) {
      return null;
    }
    if (this.jwtHelper.isTokenExpired(token)) {
      this.logout();
      return null;
    }
    return token;
  }
}
