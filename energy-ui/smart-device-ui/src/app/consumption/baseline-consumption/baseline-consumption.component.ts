/*
 * Copyright (c) 2022 Áron Katona
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from "rxjs";
import {SmartDeviceClient} from "../../../proto/com/katonaaron/energy/exposition/smartdevice/smart-device.pbsc";
import {
  GetBaselineEnergyConsumptionRequest
} from "../../../proto/com/katonaaron/energy/exposition/smartdevice/smart-device.pb";
import {filter, map} from "rxjs/operators";
import {faSpinner} from '@fortawesome/free-solid-svg-icons';
import {getTimeZoneOffset} from "../../shared/util";
import {StoreService} from "../../shared/store/store.service";

@Component({
  selector: 'app-baseline-consumption',
  templateUrl: './baseline-consumption.component.html',
  styleUrls: ['./baseline-consumption.component.css']
})
export class BaselineConsumptionComponent implements OnInit, OnDestroy {

  chartData: any[] = [];
  xAxisTicks = [...Array(24).keys()]
  xAxisLabel: string = 'Hour';
  yAxisLabel: string = 'Energy [kW]';

  faSpinner = faSpinner;

  loading = false;
  clientId: string | null = null;

  subscription: Subscription | null = null;

  constructor(private client: SmartDeviceClient,
              private storeService: StoreService) {
  }

  get showChart(): boolean {
    return this.chartData.length > 0 && this.chartData[0].series.length > 0;
  }

  ngOnDestroy(): void {
    this.clearData();
  }

  ngOnInit(): void {
    this.storeService.clientId$.subscribe(
      clientId => {
        this.clientId = clientId;
        this.updateData();
      }
    )
  }

  private fetchData(clientId: string): void {
    this.loading = true;

    const request = new GetBaselineEnergyConsumptionRequest({
      clientId: clientId
    });

    if (this.subscription) {
      this.subscription.unsubscribe();
    }

    this.subscription = this.client.getBaselineEnergyConsumption(request)
      .pipe(
        filter((energyConsumption) => {
          return !!energyConsumption.baseline;
        }),
        map(series => {
          const timeOffset = getTimeZoneOffset();
          return {
            name: "Baseline",
            series: series.baseline!.map((consumption, index) => ({
              name: (index + timeOffset) % 24,
              value: consumption
            }))
          };
        }))
      .subscribe(
        results => {
          this.chartData = [results];
          console.log("success", results);
        },
        err => {
          console.log("error", err);
        },
        () => {
          this.loading = false;
          console.log("complete");
        }
      )
  }

  private clearData() {
    if (this.subscription) {
      this.subscription.unsubscribe();
      this.subscription = null;
    }
    this.chartData = [];
  }

  private updateData() {
    if (!!this.clientId) {
      this.fetchData(this.clientId);
    } else {
      this.clearData();
    }
  }

}
