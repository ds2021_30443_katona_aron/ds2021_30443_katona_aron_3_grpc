/*
 * Copyright (c) 2022 Áron Katona
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import {Component, OnDestroy, OnInit} from '@angular/core';
import {
  GetHistoricalEnergyConsumptionRequest
} from "../../../proto/com/katonaaron/energy/exposition/smartdevice/smart-device.pb";
import {filter, map, toArray} from "rxjs/operators";
import {SmartDeviceClient} from "../../../proto/com/katonaaron/energy/exposition/smartdevice/smart-device.pbsc";
import {Subscription} from "rxjs";
import {faSpinner} from '@fortawesome/free-solid-svg-icons';
import {StoreService} from "../../shared/store/store.service";

@Component({
  selector: 'app-historical-consumption',
  templateUrl: './historical-consumption.component.html',
  styleUrls: ['./historical-consumption.component.css']
})
export class HistoricalConsumptionComponent implements OnInit, OnDestroy {
  chartData: any[] = [];
  xAxisLabel: string = 'Time';
  yAxisLabel: string = 'Energy [kW]';

  faSpinner = faSpinner;

  loading = false;
  clientId: string | null = null;
  numberOfDays = 7;

  subscription: Subscription | null = null;

  constructor(private client: SmartDeviceClient,
              private storeService: StoreService) {
  }

  get showChart(): boolean {
    return this.chartData.length > 0 && this.chartData[0].series.length > 0;
  }

  ngOnDestroy(): void {
    this.clearData();
  }

  ngOnInit(): void {
    this.storeService.clientId$.subscribe(
      clientId => {
        this.clientId = clientId;
        this.updateData();
      }
    )
  }

  onNumberOfDaysChange(numberOfDays: number) {
    this.updateData(numberOfDays);
  }

  private fetchData(clientId: string, numberOfDays: number): void {
    this.loading = true;

    if (this.subscription) {
      this.subscription.unsubscribe();
      this.subscription = null;
    }

    const request = new GetHistoricalEnergyConsumptionRequest({
      clientId: clientId,
      numberOfDays: numberOfDays
    });

    this.subscription = this.client.getHistoricalEnergyConsumption(request)
      .pipe(
        filter((energyConsumption) => {
          return !!energyConsumption.timestamp && !!energyConsumption.consumption;
        }),
        map(energyConsumption => ({
          name: energyConsumption.timestamp!.toDate(),
          value: energyConsumption.consumption!
        })),
        toArray(),
        map(series => ({
          name: "Energy Consumption [kW]",
          series: series
        }))
      )
      .subscribe(
        results => {
          this.chartData = [results];
          console.log("success", results);
        },
        err => {
          console.log("error", err);
        },
        () => {
          this.loading = false;
          console.log("complete");
        }
      )
  }

  private clearData() {
    if (this.subscription) {
      this.subscription.unsubscribe();
      this.subscription = null;
    }
    this.chartData = [];
  }

  private updateData(numberOfDays: number = this.numberOfDays) {
    if (!!this.clientId && numberOfDays >= 1) {
      this.fetchData(this.clientId, numberOfDays);
    } else {
      this.clearData();
    }
  }
}
