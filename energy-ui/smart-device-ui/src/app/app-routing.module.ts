/*
 * Copyright (c) 2022 Áron Katona
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {ErrorPageComponent} from "./error-page/error-page.component";
import {HistoricalConsumptionComponent} from "./consumption/historical-consumption/historical-consumption.component";
import {BaselineConsumptionComponent} from "./consumption/baseline-consumption/baseline-consumption.component";
import {ScheduleDeviceComponent} from "./schedule-device/schedule-device.component";
import {HomeComponent} from "./home/home.component";
import {UserRole} from "./shared/auth/user.model";
import {RoleGuardService} from "./shared/auth/role-guard.service";
import {LoginComponent} from "./shared/auth/login/login.component";

const appRoutes: Routes = [
  {path: '', component: HomeComponent},
  {
    path: 'historical',
    component: HistoricalConsumptionComponent,
    canActivate: [RoleGuardService],
    data: {expectedRole: UserRole.ROLE_CLIENT, title: 'Historical consumption'}
  },
  {
    path: 'baseline',
    component: BaselineConsumptionComponent,
    canActivate: [RoleGuardService],
    data: {expectedRole: UserRole.ROLE_CLIENT, title: 'Baseline consumption'}
  },
  {
    path: 'schedule',
    component: ScheduleDeviceComponent,
    canActivate: [RoleGuardService],
    data: {expectedRole: UserRole.ROLE_CLIENT, title: 'Schedule device'}
  },
  {path: 'login', component: LoginComponent, data: {title: 'Login'}},
  {path: 'not-found', component: ErrorPageComponent, data: {message: 'Page not found!', title: 'Not found'}},
  {path: '**', redirectTo: '/not-found'},
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}
