FROM node:16.10-alpine as builder

ARG AUTH_SERVER_DOMAIN=localhost:8080
ENV AUTH_SERVER_DOMAIN=$AUTH_SERVER_DOMAIN
ARG GRPC_SERVER_DOMAIN=localhost:9090
ENV GRPC_SERVER_DOMAIN=$GRPC_SERVER_DOMAIN

# download envsubst
RUN apk --no-cache add curl
RUN curl -L https://github.com/a8m/envsubst/releases/download/v1.2.0/envsubst-`uname -s`-`uname -m` -o envsubst && \
    chmod +x envsubst && \
    mv envsubst /usr/local/bin

# copy application and install npm dependencies
WORKDIR /app
COPY package*.json /app/
RUN npm install
COPY ./ /app/

# substitute environmental variables
RUN /usr/local/bin/envsubst < src/environments/environment.prod.ts > temp
RUN mv temp src/environments/environment.prod.ts

# build app
RUN npm run build -- --configuration production


FROM nginx:1.17-alpine
RUN apk --no-cache add curl
RUN curl -L https://github.com/a8m/envsubst/releases/download/v1.2.0/envsubst-`uname -s`-`uname -m` -o envsubst && \
    chmod +x envsubst && \
    mv envsubst /usr/local/bin


COPY --from=builder /app/nginx.conf /etc/nginx/nginx.template
CMD ["/bin/sh", "-c", "envsubst < /etc/nginx/nginx.template > /etc/nginx/conf.d/default.conf && nginx -g 'daemon off;'"]
COPY --from=builder /app/dist/smart-device-ui /usr/share/nginx/html

EXPOSE 80
