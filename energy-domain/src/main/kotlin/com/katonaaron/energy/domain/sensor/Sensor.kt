/*
 * Copyright (c) 2021 Áron Katona
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.katonaaron.energy.domain.sensor

import com.katonaaron.energy.domain.DDD
import com.katonaaron.energy.domain.DomainConstraintValidationException
import com.katonaaron.energy.domain.Validatable
import com.katonaaron.energy.domain.device.Device
import com.katonaaron.energy.domain.device.EnergyConsumption
import com.katonaaron.energy.domain.energy.Energy
import org.springframework.context.ApplicationEvent
import org.springframework.data.domain.AfterDomainEventPublication
import org.springframework.data.domain.DomainEvents
import java.util.*
import javax.persistence.*
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Size
import kotlin.jvm.Transient

@DDD.DomainEntity
@Entity
class Sensor(
    @EmbeddedId
    @AttributeOverride(name = "value", column = Column(name = "id"))
    val id: SensorId,
    description: String,
    maxValue: Energy,
) : Validatable {

    @Size(min = 3, max = 255, message = "The description must be at least 3, at most 255 characters long")
    @NotBlank(message = "Must provide a description")
    var description: String = description
        set(value) {
            field = value
            validate(this)
        }

    @Embedded
    @AttributeOverride(name = "amountSI", column = Column(name = "max_value"))
//    @Positive
    var maxValue: Energy = maxValue
        set(value) {
            field = value
            validate(this)
        }

    @OneToOne(
        mappedBy = "sensor"
    )
    var device: Device? = null
        private set

    fun attachToDevice(device: Device) {
        if (this.device != null) {
            if (this.device == device) {
                return
            } else {
                throw DomainConstraintValidationException("Sensor already has a device")
            }
        }
        this.device = device
        device.attachSensor(this)
        validate(this)
    }

    fun isAttachedToDevice(): Boolean = device != null

    fun detachFromDevice() {
        device?.let {
            device = null
            it.removeSensor()
            validate(this)
        }
    }

    fun recordEnergyConsumption(value: Energy) {
        device?.recordEnergyConsumption(value)
            ?: throw DomainConstraintValidationException("Sensor not connected to device")
    }

    fun recordEnergyConsumption(consumption: EnergyConsumption) {
        device?.recordEnergyConsumption(consumption)
            ?: throw DomainConstraintValidationException("Sensor not connected to device")
        registerEvent(EnergyConsumptionAddedEvent(id, consumption))
        device?.powerPeak?.let { peak ->
            if (peak > maxValue) {
                println("peak is big")
                registerEvent(PeakExceededSensorMaxValueEvent(id, maxValue, peak))
            }
        }
    }

    @Transient
    @javax.persistence.Transient
    private var _domainEvents: MutableList<ApplicationEvent>? = null

    val events: List<ApplicationEvent>
        @DomainEvents
        get() = _domainEvents ?: listOf()

    @AfterDomainEventPublication
    fun clearEvents() {
        _domainEvents = _domainEvents?.apply { clear() }
            ?: mutableListOf()
    }

    private fun registerEvent(event: ApplicationEvent) {
        _domainEvents = _domainEvents?.apply { add(event) }
            ?: mutableListOf(event)
    }

    init {
        validate(this)
    }

    override fun equals(other: Any?): Boolean {
        return when {
            this === other -> true
            other is Sensor -> id == other.id
            else -> false
        }
    }

    override fun hashCode(): Int = Objects.hash(id)
}
