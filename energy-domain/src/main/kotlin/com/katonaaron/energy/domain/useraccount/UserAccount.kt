/*
 * Copyright (c) 2021 Áron Katona
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.katonaaron.energy.domain.useraccount

import com.katonaaron.energy.domain.DDD
import com.katonaaron.energy.domain.Validatable
import com.katonaaron.energy.domain.client.Client
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import javax.persistence.*
import javax.validation.constraints.NotBlank

@DDD.DomainEntity
@Entity
class UserAccount(
    @EmbeddedId
    @AttributeOverride(name = "value", column = Column(name = "id"))
    val id: UserAccountId,

    @Column(unique = true)
    private val username: String,

    @field:NotBlank()
    private var password: String,

    val role: UserRole
) : Validatable, UserDetails {

    @OneToOne(
        mappedBy = "account",
        optional = true
    )
    val client: Client? = null

    fun changePassword(newPassword: String) {
        this.password = newPassword
        validate(this)
    }

    init {
        validate(this)
    }

    override fun getAuthorities(): MutableCollection<out GrantedAuthority> =
        mutableListOf(SimpleGrantedAuthority(role.name))

    override fun getPassword(): String = password

    override fun getUsername(): String = username

    override fun isAccountNonExpired(): Boolean = true

    override fun isAccountNonLocked(): Boolean = true

    override fun isCredentialsNonExpired(): Boolean = true

    override fun isEnabled(): Boolean = true
}
