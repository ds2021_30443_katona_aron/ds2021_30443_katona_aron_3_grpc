/*
 * Copyright (c) 2021-2022 Áron Katona
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.katonaaron.energy.domain.energy

import com.katonaaron.energy.domain.DDD
import com.katonaaron.energy.domain.energy.Energy.Companion.SI_UNIT
import org.springframework.stereotype.Component
import javax.persistence.Access
import javax.persistence.AccessType
import javax.persistence.Embeddable
import javax.persistence.Transient
import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext
import javax.validation.constraints.Positive
import javax.validation.constraints.PositiveOrZero

@DDD.ValueObject
@Embeddable
@Access(AccessType.FIELD)
class Energy(amount: Double, unit: EnergyUnit) : Comparable<Energy> {
    companion object {
        val SI_UNIT = EnergyUnit.J
        val ZERO = Energy(0, SI_UNIT)

        private const val kWh_IN_J = 3_600_000

        fun convertToSI(amount: Double, from: EnergyUnit): Double {
            return when (from) {
                EnergyUnit.J -> amount
                EnergyUnit.kWh -> amount * kWh_IN_J
            }
        }

        fun convertFromSI(amount: Double, to: EnergyUnit): Double {
            return when (to) {
                EnergyUnit.J -> amount
                EnergyUnit.kWh -> amount / kWh_IN_J
            }
        }
    }

    private val amountSI: Double = convertToSI(amount, unit)

    constructor(amount: Long, unit: EnergyUnit) : this(amount.toDouble(), unit)

    fun amount(unit: EnergyUnit): Double = convertFromSI(amountSI, unit)

    @Transient
    fun isPositiveOrZero(): Boolean = amountSI >= 0

    @Transient
    fun isPositive(): Boolean = amountSI > 0

    override fun toString(): String = "${convertFromSI(amountSI, EnergyUnit.kWh)} kWh"

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Energy) return false

        if (amountSI != other.amountSI) return false

        return true
    }

    override fun hashCode(): Int {
        return amountSI.hashCode()
    }

    override fun compareTo(other: Energy): Int {
        return amountSI.compareTo(other.amountSI)
    }

    operator fun plus(increment: Energy): Energy =
        Energy(
            amountSI + increment.amountSI,
            SI_UNIT
        )

    operator fun minus(decrement: Energy): Energy =
        Energy(
            amountSI - decrement.amountSI,
            SI_UNIT
        )

    operator fun div(number: Number): Energy =
        Energy(
            amountSI / number.toDouble(),
            SI_UNIT
        )
}

@Component
class PositiveOrZeroEnergyValidator : ConstraintValidator<PositiveOrZero, Energy> {
    override fun isValid(energy: Energy?, ctx: ConstraintValidatorContext?): Boolean {
        return energy?.amount(SI_UNIT)?.let { it >= 0 } ?: false
    }
}

@Component
class PositiveEnergyValidator : ConstraintValidator<Positive, Energy> {
    override fun isValid(energy: Energy?, ctx: ConstraintValidatorContext?): Boolean {
        return energy?.amount(SI_UNIT)?.let { it > 0 } ?: false
    }
}

fun Energy.amountSI() = amount(SI_UNIT)
