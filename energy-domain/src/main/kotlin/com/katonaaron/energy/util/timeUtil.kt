/*
 * Copyright (c) 2021 Áron Katona
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.katonaaron.energy.util

import java.time.*
import java.time.chrono.ChronoLocalDateTime

fun Instant.toLocalDate(): LocalDate = LocalDate.ofInstant(this, ZoneId.systemDefault())

fun Instant.toLocalDateTime(): LocalDateTime = LocalDateTime.ofInstant(this, ZoneId.systemDefault())

fun Instant.toZonedDateTime(): ZonedDateTime = atZone(ZoneId.systemDefault())

fun ChronoLocalDateTime<*>.toInstant(): Instant = this.toInstant(OffsetDateTime.now(ZoneId.systemDefault()).offset)

operator fun Instant.minus(other: Instant): Duration = Duration.between(other, this)
