/*
 * Copyright (c) 2022 Áron Katona
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.katonaaron.energy

import com.katonaaron.energy.message.SensorMessage
import com.katonaaron.energy.queue.MessageQueue
import com.katonaaron.energy.queue.RabbitMessageQueue
import com.katonaaron.energy.sensor.CsvSensor
import com.katonaaron.energy.sensor.Sensor
import kotlinx.cli.ArgParser
import kotlinx.cli.ArgType
import kotlinx.cli.required
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.*

const val PROJECT_NAME = "sensor"
const val AMQP_URL = "AMQP_URL"
const val QUEUE_NAME = "QUEUE_NAME"
const val SENSOR_ID = "SENSOR_ID"
const val SENSOR_DELAY = "SENSOR_DELAY"

fun main(args: Array<String>) {
    TimeZone.setDefault(TimeZone.getTimeZone("UTC"))

    val parser = ArgParser(PROJECT_NAME)
    val csvPath by parser.option(ArgType.String, fullName = "input", shortName = "i", description = "Input CSV file")
        .required()
    val paramSensorDelay by parser.option(
        ArgType.Int,
        fullName = "delay",
        description = "Delay between sensor updates in milliseconds"
    )
    val paramSensorId by parser.option(
        ArgType.String,
        fullName = "id",
        description = "Sensor id. If omitted, the SENSOR_ID environmental variable is considered"
    )
    val paramAmqpUrl by parser.option(
        ArgType.String,
        fullName = "amqpUrl",
        description = "The AMQP URL. If omitted, the AMQP_URL environmental variable is considered"
    )
    val paramQueueName by parser.option(
        ArgType.String,
        fullName = "queueName",
        description = "Name of the message queue. If omitted, the QUEUE_NAME environmental variable is considered"
    )
    parser.parse(args)

    val env: Map<String, String> = System.getenv()

    val sensorId = paramSensorId ?: env[SENSOR_ID] ?: throw MissingArgumentException(SENSOR_ID)
    val amqpUrl = paramAmqpUrl ?: env[AMQP_URL] ?: throw MissingArgumentException(AMQP_URL)
    val queueName = paramQueueName ?: env[QUEUE_NAME] ?: throw MissingArgumentException(QUEUE_NAME)
    val sensorDelay = paramSensorDelay?.toLong()
        ?: env[SENSOR_DELAY]?.toLong()
        ?: throw MissingArgumentException(SENSOR_DELAY)

//    println("sensorId = ${sensorId}")
//    println("amqpUrl = ${amqpUrl}")
//    println("queueName = ${queueName}")
//    println("sensorDelay = ${sensorDelay}")

    val sensor: Sensor = CsvSensor(csvPath, sensorDelay)
    val mq: MessageQueue = RabbitMessageQueue(amqpUrl, queueName)

    mq.use {
        sensor.subscribe { sensorValue ->
            val message = SensorMessage(
                ChronoUnit.MILLIS.between(Instant.EPOCH, Instant.now()),
                sensorId,
                sensorValue
            )
            mq.sendMessage(Json.encodeToString(message))
        }

        runBlocking {
            sensor.start()
        }
    }
}
