/*
 * Copyright (c) 2022 Áron Katona
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.katonaaron.energy.sensor

import com.github.doyaaaaaken.kotlincsv.dsl.csvReader
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.map

class CsvSensor(
    private val csvPath: String,
    private val delayMillis: Long
) : Sensor {
    private val callbacks = mutableListOf<(Double) -> Unit>()

    override fun subscribe(callback: (Double) -> Unit) {
        callbacks.add(callback)
    }

    override suspend fun start() {
        csvReader().openAsync(csvPath) {
            readAllAsSequence().asFlow()
                .map { it.firstOrNull() }
                .filterNotNull()
                .collect {
                    callbacks.forEach { callback ->
                        callback(it.toDouble())
                    }
                    delay(delayMillis)
                }
        }
    }
}
