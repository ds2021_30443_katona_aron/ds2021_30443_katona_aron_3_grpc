/*
 * Copyright (c) 2022 Áron Katona
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.katonaaron.energy.queue

import com.rabbitmq.client.AMQP
import com.rabbitmq.client.Channel
import com.rabbitmq.client.Connection
import com.rabbitmq.client.ConnectionFactory

class RabbitMessageQueue(
    connectionString: String,
    private val queueName: String,
) : MessageQueue {
    private val connection: Connection
    private val channel: Channel

    private val messageProperties = AMQP.BasicProperties.Builder()
        .contentType("application/json")
        .headers(
            mapOf(
                "__TypeId__" to "SensorMessage"
            )
        )
        .contentEncoding("UTF-8")
        .deliveryMode(2) // make message persistent
        .build()

    init {
        val factory = ConnectionFactory()
        factory.setUri(connectionString)
        connection = factory.newConnection()
        channel = connection.createChannel()
        channel.queueDeclare(queueName, true, false, false, null)
    }

    override fun sendMessage(message: String) {
        channel.basicPublish("", queueName, messageProperties, message.toByteArray())
        println(" [x] Sent '$message'")
    }

    override fun close() {
        channel.close()
        connection.close()
    }
}
